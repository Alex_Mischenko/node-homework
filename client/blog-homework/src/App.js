import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import PostPreview from './components/PostPreview';

class App extends Component {
    state = { posts: [] }

    componentDidMount() {
        fetch('/api/posts')
            .then(res => {
                
                return res.json();
            })
            .then(posts => this.setState({ posts }));
    }

    render() {
        return (
            <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo" />
                <h1 className="App-title">Blog</h1>
            </header>
            <p className="App-intro">Posts:</p>
                { 
                    this.state.posts.map(post => {

                        let postPreviewProps = {
                            title: post.title,
                            text: post.text,
                            author: {
                                firstName: post.author.name.first,
                                lastName: post.author.name.last
                            },
                            date: post.created
                        }
                        return(
                            PostPreview(postPreviewProps)
                        // <div key={post._id}>                           
                        //     <div>{post.title}</div>
                        //     <div>{post.text}</div>
                        //     <div>
                        //         <span>{post.created}</span>
                        //         <span>{post.author.name.first} {post.author.name.last}</span>
                        //     </div>
                        // </div>
                        );
                            
                    }) 
                }
            </div>
        );
    }
    
}

export default App;
