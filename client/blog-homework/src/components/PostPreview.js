import React from 'react';
import PropTypes from 'prop-types';

const PostPreview = ({ title, text, author, date }) => {
    return(
        <div>
            <div>{title}</div>
            <div>{text}</div>
            <div>
                <span>{date}</span>
                <span>{author.firstName} {author.lastName}</span>
            </div>
        </div>
    );
}

// PostPreview.propTypes = {
//     title: PropTypes.text.isRequired,
//     text: PropTypes.text.isRequired,
//     author: PropTypes.shape({
//         firstName: PropTypes.string,
//         lastName: PropTypes.string
//       }).isRequired,
//     date: PropTypes.date.isRequired,    
// }

export default PostPreview