var mongoose = require('mongoose');

var authorSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        first: String,
        last: String,
    },
    biograthy: String,
    age: Number,
});

var Author = mongoose.model('Author', authorSchema);

module.exports = Author;