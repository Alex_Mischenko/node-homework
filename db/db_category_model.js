var mongoose = require('mongoose');

var categorySchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: String,    
});

var Category = mongoose.model('Category', categorySchema);

module.exports = Category;