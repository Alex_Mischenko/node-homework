var mongoose = require('mongoose');

var postSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    title: String,
    text: String,
    author: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Author'
    },
    category: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Category'
    },
    created: { 
        type: Date,
        default: Date.now
    }
});

var Post = mongoose.model('Post', postSchema);

module.exports = Post;