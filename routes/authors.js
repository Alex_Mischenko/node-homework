var express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer'); // v1.0.5
var upload = multer(); // for parsing multipart/form-data
var router = express.Router();

var mongoose = require('../db/db_connection');
var Category = require('../db/db_category_model');
var Author = require('../db/db_author_model');
var Post = require('../db/db_post_model');

router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

router.get('/', function(req, res, next) {

    Author.find({})
    .exec(function(err, authors) {
        if (err) throw err;
         
        res.json(authors);
    });

});

router.post('/', upload.array(),function(req, res) {
    console.log('Recived POST request on "/" authors index page');

    console.log(req.body);
    
    var newAuthor = new Author ({
        _id: new mongoose.Types.ObjectId(),
        name: {
            first: req.body.firstName,
            last: req.body.lastName,
        },
        biograthy: req.body.biograthy,
        age: req.body.age,
    });
    newAuthor.save( (err) => {
        if (err) throw err;
    });

    res.json(newAuthor);
});

module.exports = router;