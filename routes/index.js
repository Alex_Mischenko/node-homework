var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('index', { title: 'Express' });
});

router.post('/', function(req, res) {
	console.log('Recived POST request on "/" index page');
	res.json({
		"field1": 'value1',
		"field2": 'value2',
	});
})

module.exports = router;
