var express = require('express');
var bodyParser = require('body-parser');
var multer = require('multer'); // v1.0.5
var upload = multer(); // for parsing multipart/form-data
var router = express.Router();

var mongoose = require('../db/db_connection');
var Category = require('../db/db_category_model');
var Author = require('../db/db_author_model');
var Post = require('../db/db_post_model');

router.use(bodyParser.json()); // for parsing application/json
router.use(bodyParser.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

router.get('/', function(req, res, next) {

    Post.find({}).
    populate('author').
    populate('category').
    exec(function(err, posts) {
        if (err) throw err;

        // let res = posts.map( post => {
        //     let newItem = post;
        //     newItem.author = post.author;
        //     newItem.category = post.category;
        //     return newItem;
        // })
         
        res.json(posts);
    });

});

router.post('/', upload.array(),function(req, res) {
    console.log('Recived POST request on "/" posts index page');

    console.log(req.body);
    
    var newPost = new Post ({
        _id: new mongoose.Types.ObjectId(),
        title: req.body.title,
        text: req.body.text,
        author: req.body.author,
        category: req.body.category,
    });
    newPost.save( (err) => {
        if (err) throw err;
    });

    res.json(newPost);
});

module.exports = router;